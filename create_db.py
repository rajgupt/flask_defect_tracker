from app import db
from models import Defect

# create the database and the db table
db.create_all()

# insert data
db.session.add(Defect("Launch Error", "Tcl Error on Launch"))
db.session.add(Defect("No result", "No output result"))
db.session.add(Defect("Hang", "Application Hanged"))
db.session.add(Defect("Crash", "App not responding"))
db.session.add(Defect("Multiple Warning", "Recurring license warnings"))


# commit the changes
db.session.commit()
