from flask import Flask, render_template, redirect, url_for, request, \
    session, flash
from flask_sqlalchemy import SQLAlchemy

# define the flask app
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///defects.db'

db = SQLAlchemy(app)



# import db schema
from models import *

@app.route('/')
def home():
    defects = db.session.query(Defect).all()
    return render_template("home.html", defects = defects)


@app.route('/defect/<int:defect_id>')
def show_post(defect_id):
    defect = db.session.query(Defect).filter_by(id = defect_id).first()
    print defect
    return render_template("defect.html", defect = defect)

# start the server with the 'run()' method
if __name__ == '__main__':
    app.run(debug=True)
