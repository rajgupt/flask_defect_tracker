from app import db


class Defect(db.Model):
    __tablename__ = "defects"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(1000))

    def __init__(self, title, description):
        self.title = title
        self.description = description

    def __repr__(self):
        return "defect: {}".format(self.title)

